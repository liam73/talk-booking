# Talk Booking (Scalable FastAPI Applications on AWS)

## project organisation

    ├── ci_cd               CI/CD configuration yaml files + docker image specifications to run CI/CD jobs
    ├── infrastructure      Infrastructure as code files for tools of choice (servers, subnets, load balancers, managed dbs)
    ├── lib                 Internal library packages that are deployed to private registries (PyPI, NPM, Maven)
    ├── README.md           This README file
    ├── services            Long running or end-user-facing code (apps, microservice APIs)
    └── tools               General tools

